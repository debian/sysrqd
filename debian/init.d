#! /bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          sysrqd
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO
#
# sysrqd init script - Julien Danjou <acid@debian.org>
#

DAEMON=/usr/sbin/sysrqd
DESC="sysrq daemon"
SECRET=/etc/sysrqd.secret

test -f $SECRET || { log_warning_msg "$SECRET not found, exiting ..."; exit 0; }
